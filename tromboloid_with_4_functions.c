//WAP to find the volume of a tromboloid using 4 functions.
#include <stdio.h>

float input()
{
    float p;
    printf("Enter dimension of tromboloid :\n");
    scanf("%f",&p);
    return p;
}

float volume(float p,float q,float r)
{
    float v=0;
    v=((p*q)+q)/(3*r);
    return v;
}

void output(float x,float y,float z,float vol)
{
  printf("\nThe volume of given tromboloid with height %.1f,depth %.1fand breadth %.1f is %.1f",x,y,z,vol);
}


int main()
{
    float h,d,b,vol=0;
    printf("Note : Enter height,depth and breadth respectively\n");
    h=input();
    d=input();
    b=input();
    vol=volume(h,d,b);
    output(h,d,b,vol);

    return 0;
}




