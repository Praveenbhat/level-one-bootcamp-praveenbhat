//Write a program to add two user input numbers using one function.
#include <stdio.h>

int main()
{
    float a,b,sum=0;
    printf("Enter two numbers to be added:");
    scanf("%f %f",&a,&b);
    sum=a+b;
    printf("\nThe sum of given numbers is %.2f+%.2f=%.2f",a,b,sum);

    return 0;
}



