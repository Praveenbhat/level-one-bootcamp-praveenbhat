//WAP to find the distance between two point using 4 functions.
#include <stdio.h>
#include <math.h>

struct coordinate
{
    float x;
    float y;
};

typedef struct coordinate Coordinate;
Coordinate input()
{
    Coordinate c;
    printf("\nEnter the abscissa:");
    scanf("%f",&c.x);
    printf("\nEnter the ordinate:");
    scanf("%f",&c.y);
    return c;
}
float distance(Coordinate c1,Coordinate c2)
{
    float d=0;
    d=sqrt(pow((c2.x-c1.x),2)+pow((c2.y-c1.y),2));
    return d;
}

void output(Coordinate c1,Coordinate c2,float dist)
{
    printf("The distance between the points (%.2f,%.2f) and (%.2f,%.2f) is %.2f",c1.x,c1.y,c2.x,c2.y,dist);
}

int main()
{
    float dist;
    Coordinate c1,c2;
    c1=input();
    c2=input();
    dist=distance(c1,c2);
    output(c1,c2,dist);
    

    return 0;
}


