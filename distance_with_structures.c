//WAP to find the distance between two points using structures and 4 functions.
#include <stdio.h>
#include <math.h>

float input()
{
    float p;
    printf("\nEnter the co-ordinte:");
    scanf("%f",&p);
    return p;
}

float distance(float x1,float x2,float y1,float y2)
{
    float s;
    s=sqrt(pow((x2-x1),2)+pow((y2-y1),2));
    return s;
}

void output(float x)
{
    printf("The distance between given points is %.1f",x);
}

int main()
{
    float x1,x2,y1,y2;
    float dist=0;
    printf("Note:Enter x1,x2,y1 and y2 respectively");
    x1=input();
    x2=input();
    y1=input();
    y2=input();
    dist=distance(x1,x2,y1,y2);
    output(dist);

    return 0;
}
