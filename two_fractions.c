//WAP to find the sum of two fractions.


#include <stdio.h>

struct Fraction
{
    int n;
    int d;
};

typedef struct Fraction fraction;
fraction first_fraction()
{
    fraction f;
    printf("Enter the numerator of the first fraction:");
    scanf("%d",&f.n);
    printf("\nEnter the denominator of the first fraction:");
    scanf("%d",&f.d);
    return f;
}

fraction second_fraction()
{
    
    fraction f;
    printf("Enter the numerator of the second fraction:");
    scanf("%d",&f.n);
    printf("\nEnter the denominator of the second fraction:");
    scanf("%d",&f.d);
    return f;
}

int numerator(fraction f1,fraction f2)
{
    int p;
    p=(f1.n*f2.d)+(f2.n*f1.d);
    return p;
}

int denominator(fraction f1,fraction f2)
{
    int q;
    q=f1.d*f2.d;
    return q;
}

int gcd(int a,int b)
{
    int p;   
    while(a%b>0)
    {
        p=a%b;
        a=b;
        b=p;
    }
    return b;
  
}

fraction simplify(fraction g,int r)
{
    g.n=g.n/r;
    g.d=g.d/r;
    return g;
}
void output(fraction h)
{
    printf("\nThe sum of the fractions is %d/%d",h.n,h.d);
}


int main()
{
    fraction f1,f2,f3,f4;
    int n,d,s,r;
    f1=first_fraction();
    f2=second_fraction();
    f3.n=numerator(f1,f2);
    f3.d=denominator(f1,f2);
    r=gcd(f3.n,f3.d);
    f4=simplify(f3,r);
    output(f4);
    return 0;
}
