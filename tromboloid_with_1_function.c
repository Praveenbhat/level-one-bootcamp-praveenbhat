//Write a program to find the volume of a tromboloid using one function
#include <stdio.h>

int main()
{
    float h,d,b,vol=0;
    printf("Enter the height,depth and breadth of the tromboloid:");
    scanf("%f %f %f",&h,&d,&b);
    vol=((h*d)+d)/(3*b);
    printf("The volume of given tromboloid is %.2f",vol);
    return 0;
}

