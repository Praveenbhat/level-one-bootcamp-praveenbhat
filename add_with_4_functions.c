//Write a program to add two user input numbers using 4 functions.
#include <stdio.h>
#include <math.h>

float input()
{
    float p;
    printf("\nEnter a number:");
    scanf("%f",&p);
    return p;
}

float sum(float x,float y)
{
    float s=0;
    s=(x+y);
    return s;
}

void output(float p,float q,float r)
{
    printf("The sum of the given numbers is %f+%f=%f",p,q,r);
    
}

int main()
{
    float a,b,c;
    a=input();
    b=input();
    c=sum(a,b);
    output(a,b,c);
    return 0;
}

